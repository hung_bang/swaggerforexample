package com.example.swaggerforexample.service;


import com.emailservice.model.MailContent;
import com.emailservice.service.AbstractEmailService;
import com.example.swaggerforexample.domain.User;
import com.example.swaggerforexample.domain.UserBuilder;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Override
    public User createUser(@Valid User user) {
        return UserBuilder.anUser().withId(UUID.randomUUID().toString()).withEmail(user.getEmail())
                .withFirstname(user.getFirstname())
                .withLastname(user.getLastname())
                .withPassword(user.getPassword())
                .withConfirmPassword(user.getConfirmPassword())
                .withCreatedAt(Calendar.getInstance().getTime())
                .withLastUpdatedAt(Calendar.getInstance().getTime())
                .build();
    }


}
