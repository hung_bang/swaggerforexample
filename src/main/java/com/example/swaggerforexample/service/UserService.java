package com.example.swaggerforexample.service;

import com.example.swaggerforexample.domain.User;

import javax.validation.Valid;

public interface UserService {
    User createUser(@Valid User user);
}
