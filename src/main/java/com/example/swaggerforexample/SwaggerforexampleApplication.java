package com.example.swaggerforexample;

import com.emailservice.service.VelocityEmailTemplateImpl;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.ui.velocity.VelocityEngineFactory;

import java.io.IOException;

@SpringBootApplication
@ComponentScan(value = {"com.emailservice.*","com.example.swaggerforexample.*"})
public class SwaggerforexampleApplication {
	public static void main(String[] args) {
		SpringApplication.run(SwaggerforexampleApplication.class, args);
	}

	@Autowired
	private Environment environment;

	@Bean
	public JavaMailSender javaMailSender(){
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
		javaMailSender.setUsername(environment.getProperty("mail.smtp.user"));
		javaMailSender.setPassword(environment.getProperty("mail.smtp.pass"));
		javaMailSender.setHost(environment.getProperty("mail.smtp.host"));
		javaMailSender.setPort(Integer.valueOf(environment.getProperty("mail.smtp.port")));
		return javaMailSender;
	}


	/*
     * Velocity configuration.
     */
	@Bean
	public VelocityEngine getVelocityEngine() throws VelocityException, IOException {
		VelocityEngineFactory factory = new VelocityEngineFactory();
		factory.setResourceLoaderPath("classpath:mail-template/velocity");
		return factory.createVelocityEngine();
	}

	@Bean
	public VelocityEmailTemplateImpl velocityEmailTemplate() throws IOException {
		VelocityEmailTemplateImpl velocityEmailTemplate = new VelocityEmailTemplateImpl();
		velocityEmailTemplate.setVelocityEngine(getVelocityEngine());
		return velocityEmailTemplate;
	}
}
