package com.example.swaggerforexample.controller;

import com.emailservice.model.EmailContent;
import com.emailservice.model.MailContent;
import com.emailservice.model.MailContentBuilder;
import com.emailservice.model.MailHeader;
import com.emailservice.service.AbstractEmailService;
import com.example.swaggerforexample.domain.ResBody;
import com.example.swaggerforexample.domain.User;
import com.example.swaggerforexample.service.UserService;
import com.google.common.collect.Lists;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/users")
@Api(value = "UserController", description = "Operations about User")
public class UserController extends AbstractEmailService{

    @Autowired
    private UserService userService;


    @PostMapping
    @ApiOperation(value = "Create new User", code = 201, httpMethod = "POST", response = User.class)
    @ApiResponses(
            @ApiResponse(code = 400, message = "Invalid request param.", response = String.class)
    )
    @ApiImplicitParams(
            {@ApiImplicitParam(name = "Authorization", paramType = "header")})
    public ResponseEntity<?> createUser(@RequestBody @Valid User user) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(user));
    }


    @GetMapping("/timestamp")
    public ResponseEntity testTimestamp1(){
        ResBody resBody = new ResBody();
        resBody.setCreatedAt(Calendar.getInstance().getTime());
        MailContent mailContent = MailContentBuilder.aMailContent()
                .withTo("quanhungbang@gmail.com")
                .withFrom("quanhungbang@gmail.com")
                .withSubject("test email form hung bang")
                .withText("test email from hung bang").build();
        super.sendMail(mailContent);

        Map<String, Object> model = new HashMap<>();
        model.put("receiverEmailName", "quanhungbang@gmail.com");
        model.put("senderName", "Hung Bang");
        model.put("amount", "222");
        model.put("currency", "EUR");
        model.put("transactionCode", "029292992");
        model.put("message", "029292992");

        String body = emailTemplateHelper.getEmailContent("NotificationEmailForTransferReceiver.vm", model);
        EmailContent emailContent = new EmailContent();
        emailContent.setMailBody(body);
        MailHeader mailHeader = new MailHeader();
        mailHeader.setFrom("quanhungbang@gmail.com");

        List list = new ArrayList();
        list.add("quanhungbang@gmail.com");
        mailHeader.setTo(list);
        mailHeader.setSubject("asfasdfasdfasdf");
        super.sendMail(emailContent, mailHeader);
        return ResponseEntity.ok(resBody);
    }

    @PostMapping("/timestamp")
    public ResponseEntity testTimestamp2(@RequestBody ResBody resBody){
        System.out.println("client send: "+ resBody.getCreatedAt());

        resBody.setCreatedAt(Calendar.getInstance().getTime());
        System.out.println(resBody.getCreatedAt());

        return ResponseEntity.ok(resBody.getCreatedAt());
    }

    @GetMapping("/timestamp1")
    public ResponseEntity testTimestamp12(){
        System.out.println("hungbnangngngnng");

        return ResponseEntity.ok(Calendar.getInstance().getTime());
    }
}
