package com.example.swaggerforexample.domain;

import java.util.Date;

/**
 * Created by KAI on 4/8/18.
 */

public class ResBody {

    private Date createdAt;

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
}
