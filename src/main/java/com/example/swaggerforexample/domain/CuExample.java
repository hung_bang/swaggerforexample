package com.example.swaggerforexample.domain;

/**
 * Created by KAI on 3/23/18.
 */
public interface CuExample {
    default String getProviderName() {
        return this.getClass().getSimpleName();
    }
}
