package com.example.swaggerforexample.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "User", description = "USer pojo class")
public class User implements Serializable {

    private String id;

    @ApiModelProperty(value = "Email", required = true, dataType = "String")
    @NotNull(message = "Email required")
    private String email;

    @NotEmpty(message = "Password required")
    @NotBlank(message = "Password required")
    @ApiModelProperty(value = "password", dataType = "String", required = true)
    private String password;

    private String confirmPassword;
    private String firstname;
    private String lastname;
    private Date createdAt;
    private Date lastUpdatedAt;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public void setLastUpdatedAt(Date lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    @AssertTrue(message = "Password confirmation not matching")
    public boolean isConfirmationValid()
    {
        return getConfirmPassword() != null && getConfirmPassword().equals(password);
    }


}
