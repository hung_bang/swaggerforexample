package com.example.swaggerforexample.model;

public class AppMain {
    public static void main(String[] args) {
        Car car = new FordCar();
        ShowRoom showRoom = new ShowRoom();
        showRoom.car = car;
        showRoom.show();

        Car huyndai = new HuydaiCar();
        showRoom.car = huyndai;
        showRoom.show();

        Car mazda = new MazdaCar();
        showRoom.car = mazda;
        showRoom.show();
    }
}
